#include "downloader.h"
#include <QApplication>
#include <Qt>
#include <QtCharts/QPieSeries>
#include <QtCharts/QPieSlice>
#include <QPen>
#include <QtCharts/QChartView>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	Downloader w;
	w.show();

	return a.exec();
}
