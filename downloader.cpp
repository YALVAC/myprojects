#include "downloader.h"
#include "ui_downloader.h"
#include <QCoreApplication>

Downloader::Downloader(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::Downloader)
{
	ui->setupUi(this);
	series = new QBarSeries();
	chartBar = new QChart();
	axisx= new QBarCategoryAxis();
	chartViewBar= new QChartView(chartBar);
	axisY = new QValueAxis();
	chartBar->addSeries(series);
	QStringList categories;
	categories<<"ONLINE"<<"OFFLINE";
	axisx->append(categories);
	chartBar->addAxis(axisx,Qt::AlignBottom);
	series->attachAxis(axisx);
	chartBar->addAxis(axisY,Qt::AlignLeft);
	series->attachAxis(axisY);

	PieSeriesVers = new QPieSeries();
	PieChartVers = new QChart();
	chartView = new QChartView(PieChartVers);
	PieChartVers->addSeries(PieSeriesVers);
	PieChartVers->setTitle("NC_Version Analysis");
	PieChartVers->legend()->setFont(QFont("Arial",15));

	seriesStatus= new QPieSeries();
	chartStatus = new QChart();
	chartwievStatus = new QChartView(chartStatus);
	chartStatus->addSeries(seriesStatus);
	chartStatus->setTitle("Online / Offline Analysis");
	chartStatus->legend()->setFont(QFont("Arial",15));

	onlineSeries = new QPieSeries();
	chartOnline = new QChart();
	chartwievOnline = new QChartView(chartOnline);
	chartOnline->addSeries(onlineSeries);
	chartOnline->setTitle("Online Version Analysis");
	chartOnline->legend()->setFont(QFont("Arial",15));

	offlineSeries= new QPieSeries();
	chartOffline = new QChart();
	chartwievOffline = new QChartView(chartOffline);
	chartOffline->addSeries(offlineSeries);
	chartOffline->setTitle("Offline Version Analysis");
	chartOffline->legend()->setFont(QFont("Arial",15));

	getAllLogs();
	ui->tabWidget->setTabText(0,"TABLE");
	ui->tabWidget->setTabText(1,"PIE CHART");
	ui->tabWidget->setTabText(2,"BAR CHART");
	ui->tabWidget->setTabText(3,"ORG LIST");
	counter=0;
	clickCount=0;
	forItemKeyCount=0;
	ui->loadingBar->hide();
}
using namespace QtCharts;

Downloader::~Downloader()
{
	delete ui;
}

void Downloader::getByFile()
{
	orgManager = new QNetworkAccessManager(this);
	QObject::connect(orgManager, SIGNAL(finished(QNetworkReply*)), this,SLOT(getByFileFinished(QNetworkReply*)));
	auto orgUrl = "https://mydlp-log.sparsetechnology.com/LogsView/GetByFile/" +logFileNameRes.at(counter);
	orgManager->get(QNetworkRequest(QUrl(orgUrl)));
	counter=counter+1;
}

void Downloader::getByFileAction(QString logFileName)
{
	orgActionManager = new QNetworkAccessManager(this);
	QObject::connect(orgActionManager, SIGNAL(finished(QNetworkReply*)), this,
					 SLOT(getByFileFinishedAction(QNetworkReply*)));
	auto orgActionUrl = "https://mydlp-log.sparsetechnology.com/LogsView/GetByFile/" +logFileName;
	orgActionManager->get(QNetworkRequest(QUrl(orgActionUrl)));
}

void Downloader::getAllLogs()
{
	auto manager = new QNetworkAccessManager(this);
	QObject::connect(manager, SIGNAL(finished(QNetworkReply *)), this,
					 SLOT(getAllLogsFinished(QNetworkReply *)));
	manager->get(QNetworkRequest(QUrl("https://mydlp-log.sparsetechnology.com/LogsView")));

}

void Downloader::getFileContentFromAPI(QString logFileName)
{
	apiManager = new QNetworkAccessManager(this);
	QObject::connect(apiManager, SIGNAL(finished(QNetworkReply *)), this,
					 SLOT(getFileContentFromAPIFinished(QNetworkReply *)));
	auto url = "https://mydlp-log.sparsetechnology.com/LogsView/GetRawDataByFile/" + logFileName;
	apiManager->get(QNetworkRequest(QUrl(url)));
}

void Downloader::	getByFileFinished(QNetworkReply *byFileReply)
{

	QByteArray response = byFileReply->readAll();
	if(response.isEmpty())
		return;
	if (byFileReply->error()) {
		qDebug()<< "ERROR";
		qDebug()<< byFileReply->errorString();
	}
	else {
		QString str = QString::fromUtf8(response);
		str.replace("\"","");
		str.replace("\\u0022","\"");
		QJsonDocument doc = QJsonDocument::fromJson(str.toUtf8());
		QJsonObject json = doc.object();
		org.clear();
		if(json.contains("org")){
			org = "| ORG : "+json.value("org").toString();
		}
		orgList.append(org);
		while(orgList.size()==logSizee){
			list();
			return;
		}
	}
	getByFile();
}

void Downloader::getByFileFinishedAction(QNetworkReply *byFileReply)
{
	QByteArray response = byFileReply->readAll();
	if(response.isEmpty()) {
		return;
	}
	if (byFileReply->error()){
		qDebug()<< "ERROR";
		qDebug()<< byFileReply->errorString();
	}
	else {
		QString str = QString::fromUtf8(response);
		str.replace("\"","");
		str.replace("\\u0022","\"");
		QJsonDocument doc = QJsonDocument::fromJson(str.toUtf8());
		QJsonObject json = doc.object();
		org.clear();
		if(json.contains("org")){
			org = "| ORG : "+json.value("org").toString();
		}
		ui->orgLabel->setText(org);
	}
	delete orgActionManager;
}
void Downloader::getAllLogsFinished(QNetworkReply *reply)
{
	QByteArray response = reply->readAll();
	if (response.isEmpty()){
		return;
	}
	QString str = QString::fromUtf8(response);
	if (reply->error()) {
		qDebug() << "ERROR!";
		qDebug() << reply->errorString();
	}
	else {
		QJsonDocument jsonDoc = QJsonDocument::fromJson(str.toUtf8());
		QJsonArray array = jsonDoc.array();
		QMap<QString, QMap<qint64, QString>> logs;
		foreach (const QJsonValue &value, array) {
			QString logFile = value.toString();
			logFile.replace(".log", "");
			logFile.replace("ffff", "");
			QStringList stringList = logFile.split(QRegExp("[_]"), QString::SkipEmptyParts);
			logValue lv;
			if (stringList.size() != 2)				continue;
			lv.ip = stringList[0];
			lv.ts = stringList.at(1).toLongLong();
			logs[lv.ip].insert(lv.ts, value.toString());
		}
		QMapIterator<QString, QMap<qint64, QString>> i(logs);
		while (i.hasNext()) {
			i.next();
			auto ipSend= i.key();
			QMapIterator<qint64, QString> ii(i.value());
			while (ii.hasNext())
			{
				ii.next();
				logFileName = ii.value();
			}
			logFileNameRes.append(logFileName);
			auto strDate = QDateTime::fromMSecsSinceEpoch(ii.key()).toString("dd.MM.yyyy  hh:mm:ss");
			strDate.insert(0, QString("["));
			strDate.insert(22, QString("]	"));
			auto dateIp= strDate + ipSend;
			dateIpList= dateIp.split(",",QString::SkipEmptyParts);
			for(int i =0; i<dateIpList.size(); i++){
				auto item = new QListWidgetItem(dateIpList.at(i));
				ui->listWidget->addItem(item);
			}
		}
		logSizee = logs.size();
		getByFile();
	}
}

void Downloader::getFileContentFromAPIFinished(QNetworkReply *apiReply)
{

	QMap<QString,int> version;
	QMap<QString,int> versionOnlineOffline;
	QMap<QString, int> versionOnline;
	QMap<QString, int> versionOffline;
	endpoints.clear();
	version.clear();
	versionOffline.clear();
	versionOnline.clear();
	versionOnlineOffline.clear();
	int onlineCount = 0;
	QByteArray response = apiReply->readAll();
	if (response.isEmpty())
		return;
	QString apiStr = QString::fromUtf8(response);
	apiStr.replace(QRegExp("\""),"");
	apiStr.replace(QRegExp("\\\\u0022"),"\"");
	apiStr.replace(QRegExp("\\\\"),"/");
	apiStr.replace(QRegExp("////"),"/");
	QJsonDocument doc = QJsonDocument::fromJson(apiStr.toUtf8());
	QJsonObject json = doc.object();
	QJsonValue endpointsValue = json.value("endpoints");
	if (endpointsValue.type() == QJsonValue::Array){
		QJsonArray endpointsArray = endpointsValue.toArray();
		for (QJsonValue arr: endpointsArray){
			QString v;
			QJsonObject obj = arr.toObject();
			if (obj.contains("computer")){
				ColumnName computerValue;
				computerValue.comV = obj.value("computer").toString();
				table.computer = computerValue;
			}

			if (obj.contains("ip")){
				ColumnName ipValue;
				ipValue.ipV = obj.value("ip").toString();
				table.ip = ipValue;
			}

			if (obj.contains("name")) {
				ColumnName nameValue;
				nameValue.nameV = obj.value("name").toString();
				table.name = nameValue;
			}

			if (obj.contains("nc_version")) {
				v = obj.value("nc_version").toString();
				ColumnName ncValue;
				ncValue.ncV=v;
				versionOnline[v];
				versionOffline[v];
				table.nc_version = ncValue;
				if (!version.contains(v))
					version[v] = 0;
				version[v] = version[v] + 1;
			}
			if (obj.contains("os_version")){
				ColumnName osValue;
				osValue.osV = obj.value("os_version").toString();
				table.os_version = osValue;
			}

			if (obj.contains("status")){
				bool statusBool = obj.value("status").toBool();
				table.status.statusV = statusBool ? "ONLINE" : "Offline";
				if(statusBool == true){
					onlineCount = onlineCount+1;
					versionOnline[v] = versionOnline[v] + 1;
				}
				else
					versionOffline[v] = versionOffline[v] +1;
				if(!versionOnlineOffline.contains(table.status.statusV))
					versionOnlineOffline[table.status.statusV]= 0;
				versionOnlineOffline[table.status.statusV] =  versionOnlineOffline[table.status.statusV] +1;
			}

			if (obj.contains("uuid")){
				ColumnName uuidValue;
				uuidValue.uuidV = obj.value("uuid").toString();
				table.uuid = uuidValue;
				uuidValue.uuidV.clear();
			}

			if (obj.contains("mydlpclipboard")){
				QJsonObject dlpclipboardObj = obj.value("mydlpclipboard").toObject();
				Mydlpclipboard mydlpValue;
				mydlpValue.version = dlpclipboardObj.value("version").toString();
				table.mydlpclipboard = mydlpValue;
			}

			if (obj.contains("mydlpfs")){
				QJsonObject dlpfsObj = obj.value("mydlpfs").toObject();
				Mydlpfs mydlpValue;
				mydlpValue.version = dlpfsObj.value("version").toString();
				table.mydlpfs = mydlpValue;
			}

			if (obj.contains("mydlpmail")){
				QJsonObject dlpmailObj = obj.value("mydlpmail").toObject();
				Mydlpmail mydlpValue;
				mydlpValue.version = dlpmailObj.value("version").toString();
				table.mydlpmail = mydlpValue;
			}

			if (obj.contains("mydlpprinter")){
				QJsonObject dlpprinterObj = obj.value("mydlpprinter").toObject();
				Mydlpprinter mydlpValue;
				mydlpValue.version = dlpprinterObj.value("version").toString();
				table.mydlpprinter = mydlpValue;
			}

			if (obj.contains("mydlpweb")){
				QJsonObject dlpwebObj = obj.value("mydlpweb").toObject();
				Mydlpweb mydlpValue;
				mydlpValue.version = dlpwebObj.value("version").toString();
				table.mydlpweb = mydlpValue;
			}
			endpoints.push_back(table);
		}
	}
	//org,counter,online display
	QString stringEpCount = QString::number(endpoints.size());
	QString stringOnlineCount = QString::number(onlineCount);
	int offlineUsers = endpoints.size() - onlineCount;
	QString strOfflineUsers = QString::number(offlineUsers);
	QString onOffResult = "TOTAL USERS: "+stringEpCount +"  ONLINE USERS: " + stringOnlineCount + "  OFFLINE USERS: " + strOfflineUsers;
	ui->oranLine->setText(onOffResult);
	QString result =" | EP COUNT :"+ stringEpCount + " | Online :"+ stringOnlineCount;
	series->clear();
	PieSeriesVers->clear();
	seriesStatus->clear();
	onlineSeries->clear();
	offlineSeries->clear();

	TableWidgetDisplay(endpoints);
	InfoTableWidget(version,versionOnline,versionOffline);
	ui->countOn->setText(result);
	if(endpoints.size()==0){
		ui->infoTableWidget->clear();
		delete apiManager;
		return;
	}
	piesliceVersionGraphic(version);
	piesliceOnlineOfflineGraphic(versionOnlineOffline);
	piesliceOnlineGraphic(versionOnline);
	piesliceOfflineGraphic(versionOffline);
	barGraphic(version,versionOnline,versionOffline);
	delete apiManager;
	return;
}

void Downloader::TableWidgetDisplay(QVector<MyTableStruct> tableList)
{
	QTableWidget *table = ui->tableWidget;
	table->setColumnCount(12);
	table->setRowCount(tableList.size());
	if(endpoints.size()==0){
		ui->loadingBar->close();
		ui->loadingTime->clear();
		return;
	}
	QStringList upLabels;
	upLabels
			<<"computer"
		   <<"ip"
		  <<"name"
		 <<"nc_version"
		<<"os_version"
	   <<"status"
	  <<"uuid"
	 <<"mydlpclipboard"
	<<"mydlpfs"
	<<"mydlpemail"
	<<"mydlpprinter"
	<<"mydlpweb";
	table->setHorizontalHeaderLabels(upLabels);
	//insertData
	//table display elapsed timer
	ui->loadingTime->clear();
	QElapsedTimer timer;
	timer.start();
	for(int i=0; i<tableList.size(); i++){
		ui->tableWidget->setSortingEnabled(false);
		loadingBar();
		table->setStyleSheet(
					"QTableWidget{"
					"background-color: #C0C0C0;"
					"selection-background-color: rgb(52, 101, 164);"
					"}");
		table->setSelectionMode(QAbstractItemView::SingleSelection);
		table->setSelectionBehavior(QAbstractItemView::SelectRows);

		auto ep = tableList[i];
		{
			tableItem = new QTableWidgetItem;
			tableItem->setText(QString("%1").arg(ep.computer.comV));
			table->setItem(i, 0, tableItem);
		}

		{
			tableItem = new QTableWidgetItem;
			tableItem->setText(QString("%1").arg(ep.ip.ipV));
			table->setItem(i, 1, tableItem);
		}

		{
			tableItem = new QTableWidgetItem;
			tableItem->setText(QString("%1").arg(ep.name.nameV));
			table->setItem(i, 2, tableItem);
		}

		{
			tableItem = new QTableWidgetItem;
			tableItem->setText(QString("%1").arg(ep.nc_version.ncV));
			table->setItem(i, 3, tableItem);
		}

		{
			tableItem = new QTableWidgetItem;
			tableItem->setText(QString("%1").arg(ep.os_version.osV));
			table->setItem(i, 4, tableItem);
		}

		{
			tableItem = new QTableWidgetItem;
			tableItem->setText(QString("%1").arg(ep.status.statusV));
			if(ep.status.statusV.contains("ONLINE")){
				table->setItem(i, 5, tableItem);
				tableItem->setBackground(QBrush(Qt::green));
			}
			else{
				table->setItem(i, 5, tableItem);
				tableItem->setBackground(QBrush(Qt::red));
			}

			{
				tableItem = new QTableWidgetItem();
				tableItem->setText(QString("%1").arg(ep.uuid.uuidV));
				table->setItem(i, 6, tableItem);
			}

			{
				tableItem = new QTableWidgetItem;
				tableItem->setText(QString("Version: %1").arg(ep.mydlpclipboard.version));
				table->setItem(i, 7, tableItem);
			}

			{
				tableItem = new QTableWidgetItem;
				tableItem->setText(QString("Version: %1").arg(ep.mydlpfs.version));
				table->setItem(i, 8, tableItem);
			}

			{
				tableItem = new QTableWidgetItem;
				tableItem->setText(QString("Version: %1").arg(ep.mydlpmail.version));
				table->setItem(i, 9, tableItem);
			}

			{
				tableItem = new QTableWidgetItem;
				tableItem->setText(QString("Version: %1").arg(ep.mydlpprinter.version));
				table->setItem(i, 10, tableItem);
			}

			{
				tableItem = new QTableWidgetItem;
				tableItem->setText(QString("Version: %1").arg(ep.mydlpweb.version));
				table->setItem(i, 11, tableItem);
			}
		}
		ui->loadingBar->hide();
		QString strTimer = QString::number(timer.elapsed());
		ui->loadingTime->setText(strTimer);
	}
	ui->tableWidget->setSortingEnabled(true);
	tableList.clear();
}

void Downloader::InfoTableWidget(QMap<QString, int> pieVers, QMap<QString, int> pieVersOn, QMap<QString, int> pieVersOff)
{
	QTableWidget *infoTable = ui->infoTableWidget;
	infoTable->setColumnCount(4);
	infoTable->setRowCount(pieVers.size());
	QStringList upLabels;
	upLabels<<"VersionName"<<"Total Count"<<"Online Count"<<"Offline Count";
	infoTable->setHorizontalHeaderLabels(upLabels);
	for(int i=0; i<pieVers.size(); i++){
		forItemKeyCount =1;
		{
			itemKey = new QTableWidgetItem;
			itemKey->setText(pieVers.keys().at(i));
			infoTable->setItem(i,0,itemKey);
		}

		{
			itemKey = new QTableWidgetItem;
			QString strVersionValue = QString::number(pieVers.values().at(i));
			itemKey->setText(strVersionValue);
			infoTable->setItem(i,1,itemKey);
		}

		{
			itemKey = new QTableWidgetItem;
			QString strVersionOnlineValue = QString::number(pieVersOn.values().at(i));
			itemKey->setText(strVersionOnlineValue);
			infoTable->setItem(i,2,itemKey);
		}

		{
			itemKey = new QTableWidgetItem;
			QString strVersionOfflineValue = QString::number(pieVersOff.values().at(i));
			itemKey->setText(strVersionOfflineValue);
			infoTable->setItem(i,3,itemKey);
		}
	}
}

void Downloader::list()
{
	for(int i = 0; i<orgList.size(); i++){
		auto item = new QListWidgetItem(orgList.at(i));
		ui->orgListWidget->addItem(item);
	}
	for(int i=0; i<ui->listWidget->count(); i++)
		ui->listWidget->item(i)->setToolTip(orgList.at(i));
	delete orgManager;
}


void Downloader::drawTable(QString target)
{
	if (target.isEmpty()){
		qDebug() << "File name not empty!!!!!!!!!!!";
		return;
	}
	getFileContentFromAPI(target);
	getByFileAction(target);
}

void Downloader::piesliceVersionGraphic(QMap<QString,int>vers)
{

	for(int i =0; i<vers.size(); i++)
	PieSeriesVers->append(vers.keys().at(i),vers.values().at(i));
	pieSlice0Vers = PieSeriesVers->slices().at(0);
	PieChartVers->setAnimationOptions(QChart::SeriesAnimations);
	pieSlice0Vers->setExploded(true);
	pieSlice0Vers->setLabelVisible(true);
	pieSlice0Vers->setPen(QPen(Qt::darkGreen,2));
	pieSlice0Vers->setBrush(Qt::green);
	chartView->setRenderHint(QPainter::Antialiasing);
	chartView->setParent(ui->allVersionFrame);
}

void Downloader::piesliceOnlineOfflineGraphic(QMap<QString,int>pieVersOnOff)
{
	for (int i = 0; i<pieVersOnOff.size(); i++)
		seriesStatus->append(pieVersOnOff.keys().at(i),pieVersOnOff.values().at(i));
	if(pieVersOnOff.size()==1){
		if(pieVersOnOff.contains("ONLINE")){
			QPieSlice *slice0 = seriesStatus->slices().at(0);
			chartStatus->setAnimationOptions(QChart::SeriesAnimations);
			slice0->setExploded(true);
			slice0->setLabelVisible(true);
			slice0->setPen(QPen(Qt::darkGreen,2));
			slice0->setBrush(Qt::green);
		}
		else
		{
			QPieSlice *slice0 = seriesStatus->slices().at(0);
			chartStatus->setAnimationOptions(QChart::SeriesAnimations);
			slice0->setExploded(true);
			slice0->setLabelVisible(true);
			slice0->setPen(QPen(Qt::darkGreen,2));
			slice0->setBrush(Qt::red);
		}
	}
	if(pieVersOnOff.size()==2){
		QPieSlice *slice0 = seriesStatus->slices().at(0);
		chartStatus->setAnimationOptions(QChart::SeriesAnimations);
		slice0->setExploded(true);
		slice0->setLabelVisible(true);
		slice0->setPen(QPen(Qt::darkGreen,2));
		slice0->setBrush(Qt::green);
		QPieSlice *slice1 = seriesStatus->slices().at(1);
		slice1->setExploded(true);
		slice1->setLabelVisible(true);
		slice1->setPen(QPen(Qt::darkGreen,2));
		slice1->setBrush(Qt::red);
	}
	chartwievStatus->setRenderHint(QPainter::Antialiasing);
	chartwievStatus->setParent(ui->allStatusFrame);
}

void Downloader::piesliceOnlineGraphic(QMap<QString,int>pieVersOn)
{
	for(int i =0; i<pieVersOn.size(); i++)
		onlineSeries->append(pieVersOn.keys().at(i),pieVersOn.values().at(i));
	QPieSlice *slice0 = onlineSeries->slices().at(0);
	chartOnline->setAnimationOptions(QChart::SeriesAnimations);
	slice0->setExploded(true);
	slice0->setLabelVisible(true);
	slice0->setPen(QPen(Qt::darkGreen,2));
	slice0->setBrush(Qt::green);
	chartwievOnline->setRenderHint(QPainter::Antialiasing);
	chartwievOnline->setParent(ui->onlineFrame);
}

void Downloader::piesliceOfflineGraphic(QMap<QString,int>pieVersOff)
{
	for(int i =0; i<pieVersOff.size(); i++)
		offlineSeries->append(pieVersOff.keys().at(i),pieVersOff.values().at(i));

	QPieSlice *slice0 = offlineSeries->slices().at(0);
	chartOffline->setAnimationOptions(QChart::SeriesAnimations);
	slice0->setExploded(true);
	slice0->setLabelVisible(true);
	slice0->setPen(QPen(Qt::darkGreen,2));
	slice0->setBrush(Qt::green);
	chartwievOffline->setRenderHint(QPainter::Antialiasing);
	chartwievOffline->setParent(ui->offlineFrame);
}

void Downloader::barGraphic(QMap<QString, int> barVers, QMap<QString, int> barVersOn, QMap<QString, int> barVersOff)
{
	int val = 0;
	for(int i = 0; i<barVers.size(); i++){
		if(barVersOn.values().at(i)>=val)
			val = barVersOn.values().at(i);
		if(barVersOff.values().at(i)>=val)
			val = barVersOff.values().at(i);
	}

	for(int i=0; i<barVersOn.size(); i++){
		QBarSet *seti0 = new QBarSet(barVersOn.keys().at(i));
		*seti0<<barVersOn.values().at(i)<<barVersOff.values().at(i);
		series->append(seti0);
	}
	chartBar->setTitle("ONLINE / OFFLINE BAR Graphic");
	chartBar->setAnimationOptions(QChart::SeriesAnimations);
	series->setLabelsVisible(true);
	axisY->setRange(0,val+10);
	chartBar->legend()->setVisible(true);
	chartBar->legend()->setAlignment(Qt::AlignBottom);
	chartViewBar->setRenderHint(QPainter::Antialiasing);
	chartViewBar->setParent(ui->barFrame);

}

void Downloader::on_listWidget_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
	clickCount=clickCount+1;
	ui->tableWidget->clearContents();
	endpoints.clear();
	if(current== nullptr)
		return;
	loadingBar();
	auto changeItemRow = ui->listWidget->row(current);
	Q_UNUSED(previous);
	auto file = logFileNameRes.at(changeItemRow);
	drawTable(file);
	ui->tableWidget->setSortingEnabled(true);

}

void Downloader::on_filterLine_editingFinished()
{
	QTableWidget *table = ui->tableWidget;
	QString Filter = ui->filterLine->text();
	for(int i = 0; i<table->rowCount(); i++){
		bool match = true;
		for(int j= 0; j<table->columnCount(); j++){
			QTableWidgetItem *item = table->item(i,j);
			if(item->text().contains(Filter)){
				match = false;
				break;
			}
		}
		table->setRowHidden(i,match);
	}
}

void Downloader::loadingBar()
{
	QProgressBar *loadingBar = ui->loadingBar;
	loadingBar->show();
	loadingBar->setValue(0);
	loadingBar->setStyleSheet("QProgressBar:: chunk {    background-color: #2196F3, width: 10px; margin: 0.5px;}");
	loadingBar->setMinimum(0);
	loadingBar->setMaximum(0);
	loadingBar->setValue(100);
}

void Downloader::on_refreshButton_clicked()
{
	ui->listWidget->clear();
	getAllLogs();
}

