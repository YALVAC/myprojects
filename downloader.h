﻿#ifndef DOWNLOADER_H
#define DOWNLOADER_H

#include <QMainWindow>
#include <QListWidgetItem>
#include <QListWidget>
#include <QString>
#include <QJsonValue>
#include <QTableWidgetItem>
#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QDateTime>
#include <QDebug>
#include <QMap>
#include <QVector>
#include <QString>
#include <QElapsedTimer>
#include <QLabel>
#include <QBarSet>
#include <QtCharts>
#include <QChartView>
#include <QBarSet>
#include <QBarSeries>
#include <QPieSlice>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonParseError>
#include <QMap>
#include <QStringList>
#include <QList>
#include <QMapIterator>
#include <QTableWidget>
#include <QProgressBar>
#include <QTextEdit>
#include <QColor>
#include <QBarCategoryAxis>
#include <QValueAxis>
#include <QWidget>
#include <QByteArray>

QT_BEGIN_NAMESPACE
namespace Ui { class Downloader; }
QT_END_NAMESPACE

class Downloader : public QMainWindow
{
	Q_OBJECT

	struct logValue
	{
		qint64 ts;
		QString ip;
	};

	struct ColumnName
	{
		QString comV;
		QString ipV;
		QString nameV;
		QString osV;
		QString ncV;
		QString statusV;
		QString uuidV;
	};

	struct Mydlpclipboard {
		QString ip;
		QString uuid;
		QString version;
	};

	struct Mydlpfs {
		QString ip;
		QString uuid;
		QString version;
	};

	struct Mydlpmail {
		QString ip;
		QString uuid;
		QString version;
	};

	struct Mydlpprinter {
		QString ip;
		QString uuid;
		QString version;
	};

	struct Mydlpweb {
		QString ip;
		QString uuid;
		QString version;
	};

	struct MyTableStruct {
		ColumnName computer;
		ColumnName ip;
		Mydlpclipboard mydlpclipboard;
		Mydlpfs mydlpfs;
		Mydlpmail mydlpmail;
		Mydlpprinter mydlpprinter;
		Mydlpweb mydlpweb;
		ColumnName name;
		ColumnName nc_version;
		ColumnName os_version;
		ColumnName status;
		ColumnName uuid;
	} MyTable;
	QVector<MyTableStruct> endpoints;

protected:
	QVector<MyTableStruct> defineRandomData(int indexSize);

public:
	int clickCount;
	int forItemKeyCount;
	QStringList dateIpList;
	QChart *PieChartVers;
	QPieSeries *PieSeriesVers;
	// diğer series vechartlari delet le const a al
	QPieSeries *seriesStatus;
	QChartView *chartView;
	QPieSlice *pieSlice0Vers;
	QChart *chartStatus;
	QChartView *chartwievStatus;
	QChart *chartOnline;
	QPieSeries *onlineSeries;
	QChartView *chartwievOnline;
	QPieSeries *offlineSeries;
	QChart *chartOffline;
	QChartView *chartwievOffline;
	QChart *chartBar;
	QBarSeries *series;
	QBarCategoryAxis *axisx;
	QValueAxis *axisY;
	QChartView *chartViewBar;
	QString org;
	QStringList orgList;
	int logSizee;
	int counter;
	QStringList logFileNameRes;
	QString logFileName;
	MyTableStruct table;
	QTableWidgetItem *tableItem;
	QTableWidgetItem *itemKey;
	QNetworkAccessManager *apiManager;
	QNetworkAccessManager *orgActionManager;
	QNetworkAccessManager *orgManager;
	Downloader(QWidget *parent = nullptr);
	~Downloader();
void TableWidgetDisplay(QVector<MyTableStruct> MyTable);
void InfoTableWidget(QMap<QString,int>pieVers,QMap<QString,int>pieVersOn,QMap<QString,int>pieVersOff);
void loadingBar();
void getAllLogs();
void getFileContentFromAPI(QString logFileName);
void getByFile();
void getByFileAction(QString logFileName);
void drawTable(QString target);
void piesliceVersionGraphic(QMap<QString,int>vers);
void piesliceOnlineOfflineGraphic(QMap<QString,int>pieVersOnOff);
void piesliceOnlineGraphic(QMap<QString,int>pieVersOn);
void piesliceOfflineGraphic(QMap<QString,int>pieVersOff);
void barGraphic(QMap<QString,int>barVers,QMap<QString,int>barVersOn,QMap<QString,int>barVersOff);
void list();

public slots:
void getAllLogsFinished(QNetworkReply *reply);
void getFileContentFromAPIFinished(QNetworkReply *apiReply);
void  getByFileFinished(QNetworkReply *byFileReply);
void getByFileFinishedAction(QNetworkReply *byFileReply);

private slots:
void on_listWidget_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);
void on_filterLine_editingFinished();
void on_refreshButton_clicked();

private:
	Ui::Downloader *ui;
};

#endif // DOWNLOADER_H
